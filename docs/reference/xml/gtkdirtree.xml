<?xml version="1.0"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.3//EN"
               "http://www.oasis-open.org/docbook/xml/4.3/docbookx.dtd" [
<!ENTITY % local.common.attrib "xmlns:xi  CDATA  #FIXED 'http://www.w3.org/2003/XInclude'">
<!ENTITY version SYSTEM "version.xml">
]>
<!--ENTITY index-Objects-Grouped SYSTEM "objects_grouped.sgml"-->
<refentry id="GtkDirTree">
<refmeta>
<refentrytitle role="top_of_page" id="GtkDirTree.top_of_page">GtkDirTree</refentrytitle>
<manvolnum>3</manvolnum>
<refmiscinfo>GTKEXTRA Library</refmiscinfo>
</refmeta>
<refnamediv>
<refname>GtkDirTree</refname>
<refpurpose>A directory tree widget for GTK.</refpurpose>
</refnamediv>

<refsect1 id="GtkDirTree.functions" role="functions_proto">
<title role="functions_proto.title">Functions</title>
<informaltable pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="functions_return" colwidth="150px"/>
<colspec colname="functions_name"/>
<tbody>
<row><entry role="function_type"><link linkend="GtkWidget"><returnvalue>GtkWidget</returnvalue></link>&#160;*
</entry><entry role="function_name"><link linkend="gtk-dir-tree-new">gtk_dir_tree_new</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>
<row><entry role="function_type"><link linkend="gint"><returnvalue>gint</returnvalue></link>
</entry><entry role="function_name"><link linkend="gtk-dir-tree-open-dir">gtk_dir_tree_open_dir</link>&#160;<phrase role="c_punctuation">()</phrase></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="GtkDirTree.other" role="other_proto">
<title role="other_proto.title">Types and Values</title>
<informaltable role="enum_members_table" pgwide="1" frame="none">
<tgroup cols="2">
<colspec colname="name" colwidth="150px"/>
<colspec colname="description"/>
<tbody>
<row><entry role="datatype_keyword">struct</entry><entry role="function_name"><link linkend="GtkDirTree-struct">GtkDirTree</link></entry></row>
<row><entry role="datatype_keyword">struct</entry><entry role="function_name"><link linkend="GtkDirTreeNode">GtkDirTreeNode</link></entry></row>

</tbody>
</tgroup>
</informaltable>
</refsect1>
<refsect1 id="GtkDirTree.object-hierarchy" role="object_hierarchy">
<title role="object_hierarchy.title">Object Hierarchy</title>
<screen>    <link linkend="GObject">GObject</link>
    <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="GInitiallyUnowned">GInitiallyUnowned</link>
        <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="GtkObject">GtkObject</link>
            <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="GtkWidget">GtkWidget</link>
                <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="GtkContainer">GtkContainer</link>
                    <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="GtkCList">GtkCList</link>
                        <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> <link linkend="GtkCTree">GtkCTree</link>
                            <phrase role="lineart">&#9584;&#9472;&#9472;</phrase> GtkDirTree
</screen>
</refsect1>
<refsect1 id="GtkDirTree.implemented-interfaces" role="impl_interfaces">
<title role="impl_interfaces.title">Implemented Interfaces</title>
<para>
GtkDirTree implements
 <link linkend="AtkImplementorIface">AtkImplementorIface</link> and  <link linkend="GtkBuildable">GtkBuildable</link>.</para>

</refsect1>

<refsect1 id="GtkDirTree.includes"><title>Includes</title><synopsis>#include &lt;gtkextra.h&gt;
</synopsis></refsect1>

<refsect1 id="GtkDirTree.description" role="desc">
<title role="desc.title">Description</title>
<para>It is a GtkCTree subclass that allows you to navigate the file-system.</para>

</refsect1>
<refsect1 id="GtkDirTree.functions_details" role="details">
<title role="details.title">Functions</title>
<refsect2 id="gtk-dir-tree-new" role="function">
<title>gtk_dir_tree_new&#160;()</title>
<indexterm zone="gtk-dir-tree-new"><primary>gtk_dir_tree_new</primary></indexterm>
<programlisting language="C"><link linkend="GtkWidget"><returnvalue>GtkWidget</returnvalue></link>&#160;*
gtk_dir_tree_new (<parameter><type>void</type></parameter>);</programlisting>
<para>
</para>
</refsect2>
<refsect2 id="gtk-dir-tree-open-dir" role="function">
<title>gtk_dir_tree_open_dir&#160;()</title>
<indexterm zone="gtk-dir-tree-open-dir"><primary>gtk_dir_tree_open_dir</primary></indexterm>
<programlisting language="C"><link linkend="gint"><returnvalue>gint</returnvalue></link>
gtk_dir_tree_open_dir (<parameter><link linkend="GtkDirTree"><type>GtkDirTree</type></link> *dir_tree</parameter>,
                       <parameter>const <link linkend="gchar"><type>gchar</type></link> *path</parameter>);</programlisting>
<para>Open files from directory path in dir_tree widget.</para>
<refsect3 id="gtk-dir-tree-open-dir.parameters" role="parameters">
<title>Parameters</title>
<informaltable role="parameters_table" pgwide="1" frame="none">
<tgroup cols="3">
<colspec colname="parameters_name" colwidth="150px"/>
<colspec colname="parameters_description"/>
<colspec colname="parameters_annotations" colwidth="200px"/>
<tbody>
<row><entry role="parameter_name"><para>dir_tree</para></entry>
<entry role="parameter_description"><para><link linkend="GtkDirTree"><type>GtkDirTree</type></link> widget.</para></entry>
<entry role="parameter_annotations"></entry></row>
<row><entry role="parameter_name"><para>path</para></entry>
<entry role="parameter_description"><para><link linkend="gchar"><type>gchar</type></link> path to the dir to be opened.</para></entry>
<entry role="parameter_annotations"></entry></row>
</tbody></tgroup></informaltable>
</refsect3><refsect3 id="gtk-dir-tree-open-dir.returns" role="returns">
<title>Returns</title>
<para> TRUE(succes) or FALSE(failure).</para>
</refsect3></refsect2>

</refsect1>
<refsect1 id="GtkDirTree.other_details" role="details">
<title role="details.title">Types and Values</title>
<refsect2 id="GtkDirTree-struct" role="struct">
<title>struct GtkDirTree</title>
<indexterm zone="GtkDirTree-struct"><primary>GtkDirTree</primary></indexterm>
<programlisting language="C">struct GtkDirTree;</programlisting>
<para>The GtkDirTree struct contains only private data.
It should only be accessed through the functions described below.</para>
</refsect2>
<refsect2 id="GtkDirTreeNode" role="struct">
<title>struct GtkDirTreeNode</title>
<indexterm zone="GtkDirTreeNode"><primary>GtkDirTreeNode</primary></indexterm>
<programlisting language="C">struct GtkDirTreeNode {
  gboolean scanned;
  gchar *path;
};
</programlisting>
<para>
</para>
</refsect2>

</refsect1>

</refentry>
